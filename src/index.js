import Phaser from "phaser";


//Variables i funcions comuns a totes les nostres classes
let player = ""
let sound_jump;
let coins = "";
let scoreText = ""
let score=0;
let music_bucle,sound_die
let viu
//Les classes del nostre videojoc
// Idealment hauríem de posar una en cada fitxer i importar-les aquí
class MainScene extends Phaser.Scene {
  constructor() {
    super('gameScene');
  }

  onColision() {

    
    console.log("xoque")
  }
  refreshScore(value) {

    score += value
    scoreText.setText("Score: "+ score)
    
  }
  endGame(completed = false) {
    if(! completed) { // no hem arribat al final
      this.physics.pause() // atura tot moviment
      console.log("S'atura")
      music_bucle.stop();  
      
      sound_die.play({
        volume: 1,
        loop: false
      })
      this.time.addEvent({
         delay: 4500,
         loop:false,
         callback: ()=> {
           this.scene.restart()
         }
       })
      // Podríem generar una animació de mort del personatge
       //start('EndGame');
    } else { // Hem arribat al final bé
      // Podriem Generar una animació
      this.scene.start('endScene');
    }
  }
  preload() {
    //1r cop i  1 únic cop

    this.load.image('marioTiles', 'assets/maps/tiles.png');
    this.load.image('dungeonTiles', 'assets/maps/Dungeon.png');


    this.load.tilemapTiledJSON('tilemap', 'assets/maps/mariobros_map.json');
    this.load.image('estrella', 'assets/star.png')
    this.load.image('bomba', 'assets/bomb.png')




    // this.load.image('sky', 'assets/sky.png');
    // this.load.image('ground', 'assets/platform.png');
    // this.load.image('star', 'assets/star.png');
    // this.load.image('bomb', 'assets/bomb.png');

    this.load.spritesheet('mario',
    'assets/Mario.png',
    { frameWidth: 32, frameHeight: 32, }
  );



    this.load.spritesheet('moneda',
      'assets/monedes.png',
      { frameWidth: 16, frameHeight: 16, }
    );

    this.load.audio('bucle', 'assets/sounds/MarioBrosBucle.mp3')
    this.load.audio('die', 'assets/sounds/smb_mariodie.mp3')
    this.load.audio('jumpsmall', 'assets/sounds/smb_jump-small.mp3')

    // Bandera

    this.load.spritesheet('bandera',
    'assets/banderaSprite.png',
    { frameWidth: 16, frameHeight: 96, }
  );

  }

  create() {
    //1 únic cop

    viu = true 
    music_bucle = this.sound.add('bucle')
    sound_die = this.sound.add('die')
    music_bucle.play({
      volume: 1,
      loop: true
    })



    //this.add.image(400, 300, 'sky');

    const map = this.add.tilemap('tilemap');
    let tileset = map.addTilesetImage('mariobros', 'marioTiles');
    let tileset2 = map.addTilesetImage('Dungeon', 'dungeonTiles');

    map.createLayer('background', tileset)

    let plataforma2_layer = map.createLayer('platform2', tileset2)
    let plataforma_layer = map.createLayer('platform', tileset)



    // const objectLayer = map.getObjectLayer('platformCollider');


    //objectLayer.setCollisionByExclusion([-1]);
    // this.physics.add.collider(player, objectLayer, ()=> { console.log("oeoeoeoe") })

    
    const colliders = this.physics.add.staticGroup()

   

//     // Recorre els objectes de la capa
//  objectLayer.objects.forEach(object => {
//    // Crea un rectangle Phaser per a cada objecte
//    const rectangle = this.add.rectangle(object.x, object.y, object.width, object.height);
//    colliders.add(rectangle)

//    console.log(colliders)

// //   // Personalitza les propietats del rectangle
// //   //rectangle.setFill('#ff0000');
// //   //rectangle.setStroke('#000000');
// });
// this.physics.add.collider(player, colliders, ()=>{ console.log("AAAA")})




    player = this.physics.add.sprite(480, 100, 'mario')
    //player.setScale(1.5)
    player.body.setSize(16, 32);

    plataforma_layer.setCollisionByExclusion([-1]);
    this.physics.add.collider(player, plataforma_layer)
    
    //plataforma2_layer.setCollisionByExclusion([-1]);
    //this.physics.add.collider(player, plataforma2_layer)



    this.cameras.main.startFollow(player);

    this.anims.create({
      key: 'left',
      frames: this.anims.generateFrameNumbers('mario', { start: 9, end: 11 }),
      frameRate: 10,
      repeat: -1
    })
    this.anims.create({
      key: 'turn',
      frames: [{ key: 'mario', frame: 8 }],
      frameRate: 10,
      repeat: -1
    })
    this.anims.create({
      key: 'right',
      frames: this.anims.generateFrameNumbers('mario', { start: 9, end: 11 }),
      frameRate: 10,
      repeat: -1
    })
    this.anims.create({
      key: 'jump',
      frames: this.anims.generateFrameNumbers('mario', { start: 12, end: 14 }),
      frameRate: 10,
      repeat: -1
    })

    
    
    // Afegir monedes
    const collectables_layer = map.getObjectLayer('collectables')
    //this.physics.add.overlap(player,collectables_layer,()=> console.log("AAAAAA"))

    // Assuming 'capaColisiones' is the name of your collision layer in Tiled

    // Assuming 'capaColisiones' is the name of your collision layer in Tiled
    //var collider = this.physics.add.collider(player, map.getLayer('collectables'));

    const collectables = this.physics.add.staticGroup()

    collectables_layer.objects.forEach(moneda => {


      var spriteMoneda = this.add.sprite(moneda.x, moneda.y, 'moneda');
      collectables.add(spriteMoneda).setDepth(-1)
      //this.physics.add.overlap(spriteMoneda, player, (p,s)=> { console.log("AAA"); console.log(p); p.visible=false; p.body.destroy() }, null, this);

      this.anims.create({
        key: 'moneda',
        frames: this.anims.generateFrameNumbers('moneda', { start: 0, end: 3 }),
        frameRate: 10,
        repeat: -1
      })
      
      
      spriteMoneda.anims.play('moneda', true)



      //this.physics.enable(collectables, true);
      //this.physics.add.overlap(player, collectables, () => console.log("AAAAAA"), null, this);

      //player.addOverlap(collectables_layer,this.onColision)

      

       
      })



    const estrelles = this.physics.add.staticGroup();

    estrelles.create(400,100,"estrella")
    estrelles.create(500,100,"estrella")
    estrelles.create(600,100,"estrella")
   
    const onOverlap = (jugador,colleccionable)=>{
      // Estels
      this.refreshScore(10) // Incrementem puntuació en 10
      
      colleccionable.disableBody(true, true);
      
      console.log("AAAAA")

    }
    const onOverlapLayer = (jugador,colleccionable)=>{
      
      if (!colleccionable.collected)
      {

        colleccionable.visible=false
        colleccionable.collected=true
        this.refreshScore(1)


      }
    
      //colleccionable.body.destroy();
      console.log("AAAAA")

    }

    this.physics.add.overlap(player, estrelles, onOverlap);
    this.physics.add.overlap(player, collectables, onOverlapLayer);





  //   const birds = this.physics.add.staticGroup({
  //     key: 'bomba',
  //     repeat: 11,
  //     setXY: { x: 450, y: 110, stepX: Phaser.Math.Between(100, 150) }
  //  });

  const bomba = this.physics.add.staticGroup({
    key: 'bomba',
    repeat: 11,
    // setXY: {
    //   x: 450,
    //   y: 110,
    //   stepX: 70
    // },
  })
  // Comentar si stepX es descomenta
  bomba.children.iterate((bomb) => {
     bomb.setPosition(Phaser.Math.Between(500, 2500), Phaser.Math.Between(100,150));
  });

   
  scoreText = this.add.text(300,-50,"Score: 0",{ fontSize: '32px', fill:'#000'})


  // const camera = this.cameras.main; // Obté la càmera principal

  // camera.ignore(scoreText)  

  const bandera = this.physics.add.staticGroup();

  bandera.create(180,160,"bandera")
  bandera.scaleY=40



  }


  update() {
    //De forma repetida, un i un altre cop

    console.log(player.body.x)

    var cursors = this.input.keyboard.createCursorKeys()

    if (cursors.left.isDown) {
      player.setVelocityX(-160)
      player.anims.play('left', true)
      player.setFlipX(true);

    }
    else if (cursors.right.isDown) {
      player.setVelocityX(160)
      player.anims.play('right', true)
      player.setFlipX(false);
    }

    else {

      if (player.body.onFloor())
      {

        player.setVelocityX(0)
        player.anims.play('turn', true)
      }
     
    }

    if (cursors.up.isDown && player.body.onFloor()) {

      player.setVelocityY(-280)

      player.anims.play('jump', true)



      // jumpsmall
      sound_jump = this.sound.add('jumpsmall')
      sound_jump.play({
        volume: 1,
        loop: false
      })

    }
    scoreText.x = player.body.position.x
    scoreText.y = player.body.position.y - 200

    if (player.body.position.y > 400 && viu ) {
      viu = false
      this.endGame(false)
    }



  }
  
}

class Menu extends Phaser.Scene {
  constructor() {
    super('menuScene');
  }

  preload() {

  }

  create() {

  }

  update() {

  }
}

class Level extends Phaser.Scene {
  constructor() {
    super('levelScene');
  }

  preload() {

  }

  create() {

  }

  update() {

  }
}

class Mode extends Phaser.Scene {
  constructor() {
    super('modeScene');
  }

  preload() {

  }

  create() {

  }

  update() {

  }
}

class Controls extends Phaser.Scene {
  constructor() {
    super('controlsScene');
  }

  preload() {

  }

  create() {

  }

  update() {

  }
}

class EndGame extends Phaser.Scene {
  constructor() {
    super('endScene');
  }

  preload() {

    console.log("EEEEE")

  }

  create() {

    scoreText = this.add.text(200,100,"Final del Joc",{ fontSize: '48px', fill:'#000'})




  }

  update() {

  }
}

//Configuración genérica del videojuego
const config = {
  type: Phaser.AUTO,
  width: 800,
  height: 600,
  backgroundColor: '#ffffac',
  //scene: [Menu, MainScene, Level, Mode, Controls, EndGame], //La primera escena és per on entra el joc
  scene: [MainScene, Menu, Level, Mode, Controls, EndGame], //La primera escena és per on entra el joc
  // scale: {
  //     mode: Phaser.Scale.FIT
  // },
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 300 }
    }
  },
}

//Inicialización del objeto Phaser
new Phaser.Game(config);