# PUNT 6. Els enemics i els dispars

Aquesta part us la deixarem per a que la desenvolupeu a la vostra manera, sense documentció inicial.

Penseu que un enemic no és més que un sprite o diversos sprites que es mouen sols per la pantalla, sense intervenció nostra. És com el nostre jugador, però anant a la seva...

Observa que a la rúbrica et marquem la creació de dues tipologies de personatges , un amb un moviment fix o aleatori, però independent del nostre jugador i un segon que persegueix al jugador quan es troba a proper d'ell. 

A més a més, heu de fer que el vostre jugador dispari "boles". Aquesta és la part més complexa, però de ben segur trobaràs molts tutorials per internet o amb l'ajuda d'una IA

## Activitat 6

Genera el codi per a disparar "boles" als enemics. Crea a més enemics de almenys dues tipologies (perseguidors, moviment fix o aleatori)

